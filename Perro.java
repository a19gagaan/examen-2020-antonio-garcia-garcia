/**
 * Clase Perro
 * @author Antonio Garcia Garcia
 * @version 0.0.1
 */

/** 
 * Clase Perro
 */
public class Perro {

  private String ladra = "Guau";
		
  /** 
	* Constructor de la clase Perro
	*/
  protected Perro() {
				
	}

  /** 
  * Método ladrar
  */
	void ladrar() {
		System.out.println(ladra);
	}

}
