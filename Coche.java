/**
 * Clase Coche
 * @author Antonio Garcia Garcia
 * @version 0.0.1
 */
/*Refactorización
Extrae una superclase Vehículo con los campos
	num_serie
	fabricante
	color
y los métodos
	getNum_serie(), setNum_serie()
	getFabricante(), setFabricante()
	getColor(), setColor()*/

/**
 * Enumeracion de colores
 */
enum Color {
	ROJO, AZUL, VERDE, AMARILLO, NARANJA
};

/** 
 * Clase coche
 */
public class Coche extends Vehiculo {
	private int cilindrada;
	/**
	 * Constructor de la clase coche
	 * @param num_serie
	 * @param cilindrada
	 * @param fabricante
	 * @param color
	 */
	protected Coche(int num_serie, int cilindrada, String fabricante, Color color) {
		this.num_serie = num_serie;
		this.cilindrada = cilindrada;
		this.fabricante = fabricante;
		this.color = color;
	}

	/**
	 * Método getCilindrada
	 * @return cilindridada 
	 */
	public int getCilindrada() {
		return cilindrada;
	}

	/**
   * Método setCilindrada
   * @param cilindrada
   */
	public void setCilindrada(int cilindrada) {
		this.cilindrada = cilindrada;
	}

}
